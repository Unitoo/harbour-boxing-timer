harbour-boxing-timer
====================
Boxing timer is a customizable timer for fighting sports like boxe. Uses Sailfish Silica Qt components.

Maintainers
-----------
- Claudio Maradonna (claudio@unitoo.pw)

Designers
---------
- Stefano Amandonico (info@grafixprint.it)

Externals
---------
A grateful thanks to [jgibbon/slumber](https://github.com/jgibbon/slumber) for SleepBlank.qml to help fix ScreenBlanking issue.

License
-------
Licensed under GPLv3 (see COPYING).
Logo is licensed under CC BY-NC-SA 4.0
