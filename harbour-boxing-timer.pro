TARGET = harbour-boxing-timer

DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DEFINES += APP_RELEASE=\\\"$$RELEASE\\\"

CONFIG += sailfishapp
CONFIG += sailfishapp_i18n

QT += core qml multimedia dbus

QMAKE_TARGET_COMPANY = Unitoo
QMAKE_TARGET_PRODUCT = Boxing Timer
QMAKE_TARGET_DESCRIPTION = A customizable timer for fighting sports like boxe
QMAKE_TARGET_COPYRIGHT = Copyright © 2017-2019 Claudio Maradonna <claudio[at]unitoo[dot]name>

SOURCES += src/harbour-boxing-timer.cpp \
    src/boxingtimer.cpp \
    src/boxingsettings.cpp

OTHER_FILES += qml/harbour-boxing-timer.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-boxing-timer.spec \
    rpm/harbour-boxing-timer.yaml \
    harbour-boxing-timer.desktop \
    translations/*.ts

SAILFISHAPP_ICONS += 86x86 108x108 128x128 172x172 256x256

DISTFILES += \
    qml/ScreenBlank.qml \
    qml/pages/TimerPage.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/js/converter.js \
    qml/pages/PresetList.qml \
    qml/pages/AboutPage.qml \
    COPYING \
    rpm/harbour-boxing-timer.changes \
    qml/pages/js/helpers.js \
    README.md \
    icons/86x86/harbour-boxing-timer.png \
    icons/108x108/harbour-boxing-timer.png \
    icons/128x128/harbour-boxing-timer.png \
    icons/172x172/harbour-boxing-timer.png \
    icons/256x256/harbour-boxing-timer.png

HEADERS += \
    src/boxingtimer.h \
    src/boxingsettings.h

RESOURCES += \
    harbour-boxing-timer.qrc

TRANSLATIONS += \
    translations/harbour-boxing-timer-en_US.ts \
    translations/harbour-boxing-timer-it_IT.ts

translations.name = Translations
translations.input = TRANSLATIONS
translations.output = $$_PRO_FILE_PWD_/translations/${QMAKE_FILE_BASE}.qm
translations.commands = $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN}
translations.CONFIG = no_link
QMAKE_EXTRA_COMPILERS += translations
PRE_TARGETDEPS += compiler_translations_make_all
