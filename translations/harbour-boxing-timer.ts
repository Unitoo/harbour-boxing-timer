<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>
&quot;Boxing Timer&quot; is a customizable timer for fighting sports like boxe.

You can set duration, rest and number of rounds. If you set 0 rounds, timer loops and count number of rounds.
You can add inner timers too.

You can save, load and delete your settings giving a name to them.
                </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Developers and sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>
Copyright (C) 2017-2019 Claudio Maradonna
Unitoo Team, Italy

Sources at: https://gitlab.com/unitoo/harbour-boxing-timer
                </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="112"/>
        <source>Artists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="128"/>
        <source>
Stefano Amandonico
GrafixPrint, Italy

info@grafixprint.it
http://www.grafixprint.it
                </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="139"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="155"/>
        <source>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.&lt;br&gt;&lt;br&gt;

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.&lt;br&gt;&lt;br&gt;

You should have received a copy of the GNU General Public License
along with this program.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;br&gt;&lt;br&gt;&lt;br&gt;


All artworks are licensed under a &lt;a href=&quot;https://creativecommons.org/licenses/by-sa/4.0/&quot;&gt;Creative Commons Attribution-ShareAlike 4.0 International&lt;/a&gt;
                </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PresetList</name>
    <message>
        <location filename="../qml/pages/PresetList.qml" line="17"/>
        <source>Saved presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PresetList.qml" line="38"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PresetList.qml" line="57"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="66"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="86"/>
        <source>Add inner time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="91"/>
        <source>Remove last inner time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="108"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="110"/>
        <source>Round&apos;s settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="117"/>
        <source>Preset name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="133"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="146"/>
        <source>Rest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="159"/>
        <source>Total rounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="163"/>
        <source>Inner timers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimerPage</name>
    <message>
        <location filename="../qml/pages/TimerPage.qml" line="47"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TimerPage.qml" line="56"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TimerPage.qml" line="67"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TimerPage.qml" line="71"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TimerPage.qml" line="84"/>
        <source>Boxing timer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
