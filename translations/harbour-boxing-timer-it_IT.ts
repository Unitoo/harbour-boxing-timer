<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Chi siamo</translation>
    </message>
    <message>
        <source>
&quot;Boxing Timer&quot; is a customizable timer for fighting sports like boxe.

You can set duration, rest and number of rounds. If you set 0 rounds, timer loops and count number of rounds.
You can add inner timers too.

You can save, load and delete your settings giving a name to them.
                </source>
        <translation>
&quot;Boxing Timer&quot; è un timer personalizzabile per gli sport da combattimento come la boxe.

Puoi personalizzare la durata, il riposo e il numero di round. Se imposti 0 round, il timer va in loop e conta il numero di round effettuati.
Puoi creare anche dei round intermedi.

Puoi salvare, caricare e cancellare le tue impostazioni impostando un nome a esse.
                </translation>
    </message>
    <message>
        <source>Developers and sources</source>
        <translation>Sviluppatori e sorgenti</translation>
    </message>
    <message>
        <source>Artists</source>
        <translation>Artisti</translation>
    </message>
    <message>
        <source>
Stefano Amandonico
GrafixPrint, Italy

info@grafixprint.it
http://www.grafixprint.it
                </source>
        <translation>
Stefano Amandonico
GrafixPrint, Italia

info@grafixprint.it
http://www.grafixprint.it
                </translation>
    </message>
    <message>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <source>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.&lt;br&gt;&lt;br&gt;

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.&lt;br&gt;&lt;br&gt;

You should have received a copy of the GNU General Public License
along with this program.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;br&gt;&lt;br&gt;&lt;br&gt;


All artworks are licensed under a &lt;a href=&quot;https://creativecommons.org/licenses/by-sa/4.0/&quot;&gt;Creative Commons Attribution-ShareAlike 4.0 International&lt;/a&gt;
                </source>
        <translation>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.&lt;br&gt;&lt;br&gt;

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.&lt;br&gt;&lt;br&gt;

You should have received a copy of the GNU General Public License
along with this program.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;br&gt;&lt;br&gt;&lt;br&gt;


All artworks are licensed under a &lt;a href=&quot;https://creativecommons.org/licenses/by-sa/4.0/&quot;&gt;Creative Commons Attribution-ShareAlike 4.0 International&lt;/a&gt;
                </translation>
    </message>
    <message>
        <source>
Copyright (C) 2017-2019 Claudio Maradonna
Unitoo Team, Italy

Sources at: https://gitlab.com/unitoo/harbour-boxing-timer
                </source>
        <translation>
Copyright (C) 2017-2019 Claudio Maradonna
Unitoo Team, Italia

Sorgenti: https://gitlab.com/unitoo/harbour-boxing-timer
                </translation>
    </message>
</context>
<context>
    <name>PresetList</name>
    <message>
        <source>Saved presets</source>
        <translation>Presets salvati</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Sto eliminando</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Add inner time</source>
        <translation>Aggiungi tempo intermedio</translation>
    </message>
    <message>
        <source>Remove last inner time</source>
        <translation>Rimuovi ultimo tempo intermedio</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Round&apos;s settings</source>
        <translation>Impostazioni del round</translation>
    </message>
    <message>
        <source>Preset name</source>
        <translation>Nome del preset</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <source>Rest</source>
        <translation>Riposo</translation>
    </message>
    <message>
        <source>Total rounds</source>
        <translation>Totale round</translation>
    </message>
    <message>
        <source>Inner timers</source>
        <translation>Tempi intermedi</translation>
    </message>
</context>
<context>
    <name>TimerPage</name>
    <message>
        <source>About</source>
        <translation>Chi siamo</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Resetta</translation>
    </message>
    <message>
        <source>Boxing timer</source>
        <translation>Boxing timer</translation>
    </message>
</context>
</TS>
