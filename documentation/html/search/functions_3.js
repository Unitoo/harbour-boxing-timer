var searchData=
[
  ['getinnertimer',['getInnerTimer',['../class_boxing_settings.html#a7ddc31fa4c9bea682674cb938d2d9e24',1,'BoxingSettings']]],
  ['getinnertimers',['getInnerTimers',['../class_boxing_settings.html#abdabae9cd200706c2d207ada15b1c04c',1,'BoxingSettings']]],
  ['getpreset',['getPreset',['../class_boxing_settings.html#a2d03c793ed7ad6e97894e21b1f85105b',1,'BoxingSettings::getPreset()'],['../class_boxing_timer.html#aae9b18b16d38e1dd98a33ff0d0bed959',1,'BoxingTimer::getPreset()']]],
  ['getpresetname',['getPresetName',['../class_boxing_settings.html#ac0641004a9f244dcce8056c722685065',1,'BoxingSettings']]],
  ['getrestmilliseconds',['getRestMilliseconds',['../class_boxing_settings.html#a84fd6052ef19488b015638c68fa2be72',1,'BoxingSettings']]],
  ['getroundmilliseconds',['getRoundMilliseconds',['../class_boxing_settings.html#a2b9ff4ab1dbd14f5e8f37b88677e73f4',1,'BoxingSettings']]],
  ['getrounds',['getRounds',['../class_boxing_settings.html#ae57079ed791d793bfb310712063b1dab',1,'BoxingSettings']]],
  ['getstatus',['getStatus',['../class_boxing_timer.html#a091d7fb11525455766c19d6b9c250fa4',1,'BoxingTimer']]]
];
