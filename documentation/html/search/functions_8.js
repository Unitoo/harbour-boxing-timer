var searchData=
[
  ['savepreset',['savePreset',['../class_boxing_settings.html#a8402eca09f44d4b3773ac1fd12e8dcda',1,'BoxingSettings']]],
  ['setpreset',['setPreset',['../class_boxing_settings.html#a71e6d017856266fa95b07ed0030ed05b',1,'BoxingSettings']]],
  ['setrestmilliseconds',['setRestMilliseconds',['../class_boxing_settings.html#ad7c283d2b4ebe1333c1680a0fd19cb0c',1,'BoxingSettings']]],
  ['setroundmilliseconds',['setRoundMilliseconds',['../class_boxing_settings.html#aaa691ab3a2fa5d1812975207b7e9f9ed',1,'BoxingSettings']]],
  ['setrounds',['setRounds',['../class_boxing_settings.html#a5fee80840f4bfe8cb0fa95dea161e586',1,'BoxingSettings']]],
  ['setstatus',['setStatus',['../class_boxing_timer.html#aa097f85ad7fd3602cc2583c3cc6c4d5e',1,'BoxingTimer']]],
  ['start',['start',['../class_boxing_timer.html#a64ae1d3a8bfe52b225658bf4121dcdfa',1,'BoxingTimer']]],
  ['statuschanged',['statusChanged',['../class_boxing_timer.html#ab3d16c454a2fac620bb84721c105485a',1,'BoxingTimer']]],
  ['stop',['stop',['../class_boxing_timer.html#aa294afc288608f91eb00d1680db67738',1,'BoxingTimer']]]
];
