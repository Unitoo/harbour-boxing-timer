var searchData=
[
  ['boxingsettings',['BoxingSettings',['../class_boxing_settings.html',1,'BoxingSettings'],['../class_boxing_settings.html#a62af206ef42670b751e0cec59c206896',1,'BoxingSettings::BoxingSettings()'],['../class_boxing_settings.html#a6dc7071565573dbd59d3eb94613b2433',1,'BoxingSettings::BoxingSettings(const int &amp;rounds, const int &amp;roundMilliseconds, const int &amp;restMilliseconds)']]],
  ['boxingsettings_2ecpp',['boxingsettings.cpp',['../boxingsettings_8cpp.html',1,'']]],
  ['boxingsettings_2eh',['boxingsettings.h',['../boxingsettings_8h.html',1,'']]],
  ['boxingtimer',['BoxingTimer',['../class_boxing_timer.html',1,'BoxingTimer'],['../class_boxing_timer.html#ad5a57c7272222f91a75363bdad6543ba',1,'BoxingTimer::BoxingTimer()']]],
  ['boxingtimer_2ecpp',['boxingtimer.cpp',['../boxingtimer_8cpp.html',1,'']]],
  ['boxingtimer_2eh',['boxingtimer.h',['../boxingtimer_8h.html',1,'']]]
];
