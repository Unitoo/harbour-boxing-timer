#include "boxingsettings.h"

#include <QString>
#include <QVariant>

BoxingSettings::BoxingSettings() :
    mRounds(DefaultRounds),
    mRoundMilliseconds(DefaultRoundMilliseconds),
    mRestMilliseconds(DefaultRestMilliseconds) {
    this->setPreset(this->value("lastpreset").toString());
}

BoxingSettings::BoxingSettings(const int &rounds, const int &roundMilliseconds, const int &restMilliseconds) :
    mRounds(rounds),
    mRoundMilliseconds(roundMilliseconds),
    mRestMilliseconds(restMilliseconds),
    mPreset(DefaultPreset),
    mPresetName(DefaultPresetName) {
}

void BoxingSettings::setDefaultPreset() {
    this->mPreset = DefaultPreset;
    this->mPresetName = DefaultPresetName;
}

void BoxingSettings::setPreset(const QString &preset) {
    if (preset == DefaultPreset) {
        this->setDefaultPreset();
    } else {
        QString newPreset = this->convertRichNameToGroup(preset);

        if (this->childGroups().contains(newPreset)) {
            this->mPreset = newPreset;
        } else {
            this->setDefaultPreset();
        }
    }

    this->setValue("lastpreset", this->mPreset);
    this->loadPreset();

    emit presetChanged();
}

void BoxingSettings::loadPreset() {
    if (this->mPreset == DefaultPreset) {
        this->mRounds = DefaultRounds;
        this->mRoundMilliseconds = DefaultRoundMilliseconds;
        this->mRestMilliseconds = DefaultRestMilliseconds;
        this->mInnerTimers.clear();
    } else {
        this->beginGroup(this->mPreset);
        this->mPresetName = this->value("name").toString();
        this->mRounds = this->value("rounds").toInt();
        this->mRoundMilliseconds = this->value("roundmilliseconds").toInt();
        this->mRestMilliseconds = this->value("restmilliseconds").toInt();

        this->mInnerTimers = this->value("innerTimers").value<QList<int>>();

        this->endGroup();
    }
}

void BoxingSettings::savePreset(const QString &name, const int &rounds, const int &roundMilliseconds, const int &restMilliseconds, const QList<int> &innerTimers) {
    QString strippedName = this->convertRichNameToGroup(name);

    this->beginGroup(strippedName);
    this->setValue("name", name);
    this->setValue("rounds", rounds);
    this->setValue("roundmilliseconds", roundMilliseconds);
    this->setValue("restmilliseconds", restMilliseconds);
    this->setValue("innerTimers", QVariant::fromValue(innerTimers));
    this->endGroup();

    this->setPreset(strippedName);
}

void BoxingSettings::deletePreset(const QString &name) {
    QString strippedName = this->convertRichNameToGroup(name);

    this->beginGroup(strippedName);
    this->remove("");
    this->endGroup();

    this->setPreset(DefaultPreset);

    emit presetChanged();
}

const QStringList BoxingSettings::presetsList() {
    QStringList presets;

    foreach (const QString &group, this->childGroups()) {
        presets.append(this->value(group + "/name").toString());
    }

    return presets;
}

int BoxingSettings::getInnerTimer(const int &index) {
    return this->mInnerTimers.value(index);
}
