import QtQuick 2.2
import Sailfish.Silica 1.0

import "js/helpers.js" as Helper

Page {
    onStatusChanged: {
        if (status === PageStatus.Activating) {
            var list = settings.presetsList()
            for(var key in list) {
                listModel.append({ name: list[key] });
            }
        }
    }

    Column {
        PageHeader { title: qsTr("Saved presets") }

        width: parent.width; height: parent.height

        spacing: Theme.paddingLarge

        SilicaListView {
            id: presetsList

            width: parent.width; height: parent.height

            VerticalScrollDecorator { flickable: presetsList }

            model: listModel
            delegate: ListItem {
                id: listItem

                width: parent.width

                ListView.onRemove: animateRemoval(listItem)
                function remove() {
                     remorseAction(qsTr("Deleting"), function() {
                         settings.deletePreset(presetsList.model.get(index).name)

                         if (settings.presetsList().length < 1) {
                             pageStack.popAttached()
                         } else {
                             presetsList.model.remove(index)
                         }
                     })
                }

                onClicked: {
                    settings.preset = presetsList.model.get(index).name

                    pageStack.pop(firstPage)
                }

                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Delete")

                        onClicked: remove()
                    }
                }

                Label {
                    text: name

                    color: highlighted ?
                               Theme.highlightColor :
                               Theme.primaryColor

                    anchors {
                        left: parent.left
                        right: parent.right

                        verticalCenter: parent.verticalCenter

                        margins: Theme.paddingLarge
                    }
                }
            }

            ListModel {
                id: listModel
            }
        }
    }
}
