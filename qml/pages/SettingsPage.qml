/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.2
import Sailfish.Silica 1.0
import Pw.Unitoo.Backend 1.0

import "js/converter.js" as Converter
import "js/helpers.js" as Helper

Page {
    id: settingsPage

    allowedOrientations: Orientation.Portrait

    property string mPreset: settings.preset

    property string mPresetName: settings.presetName
    property int mRounds: settings.rounds
    property int mRoundMs: settings.roundsMs
    property int mRestMs: settings.restMs

    property var mListOfInnerTimers: settings.innerTimers

    onStatusChanged: {
        if (status === PageStatus.Active) {
            Helper.pushPresetListPage()
            Helper.generateSlidersForInnerTimers()
        }
    }

    SilicaFlickable {
        id: flickable

        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Save")

                onClicked: {
                    settings.savePreset(
                        presetName.text,
                        roundsSlider.value,
                        roundsTimeSlider.value,
                        Converter.sToMs(restSlider.value),
                        Helper.getListOfInnerTimers()
                    )

                    boxingTimer.loadPreset(presetName.text)

                    Helper.pushPresetListPage();
                }
            }
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Add inner time")
                onClicked: Helper.addSliderForInnerTime()
            }

            MenuItem {
                text: qsTr("Remove last inner time")
                onClicked: Helper.removeLastSliderFromInnerTimers()

                visible: mListOfInnerTimers.length
            }
        }

        contentHeight: settingsColumn.height

        VerticalScrollDecorator { flickable: flickable }

        Column {
            id: settingsColumn

            width: settingsPage.width
            spacing: Theme.paddingLarge

            PageHeader { title: qsTr("Settings") }

            SectionHeader { text: qsTr("Round's settings") }

            TextField {
                id: presetName

                width: parent.width

                label: qsTr("Preset name")
                text: mPresetName
            }

            Slider {
                id: roundsTimeSlider

                width: parent.width

                minimumValue: Converter.sToMs(30)
                maximumValue: Converter.sToMs(300)
                stepSize: Converter.sToMs(10)

                value: mRoundMs
                valueText: Converter.msToTime(value)

                label: qsTr("Duration")
            }

            Slider {
                id: restSlider

                width: parent.width

                minimumValue: 5; maximumValue: 60; stepSize: 5

                value: Converter.msToS(mRestMs)
                valueText: value + "s"

                label: qsTr("Rest")
            }

            Slider {
                id: roundsSlider

                width: parent.width

                minimumValue: 0; maximumValue: 15; stepSize: 1

                value: mRounds
                valueText: value

                label: qsTr("Total rounds")
            }

            SectionHeader {
                text: qsTr("Inner timers")

                visible: mListOfInnerTimers.length
            }
        }
    }
}
