import QtQuick 2.2
import Sailfish.Silica 1.0

Page {
    id: aboutPage

    SilicaFlickable {
        id: aboutFlickable

        anchors.fill: parent

        contentHeight: column.height

        VerticalScrollDecorator { flickable: aboutFlickable }

        Column {
            id: column

            width: aboutPage.width

            spacing: Theme.paddingLarge

            PageHeader { title: qsTr("About") }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right

                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }

                wrapMode: Text.Wrap

                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("
\"Boxing Timer\" is a customizable timer for fighting sports like boxe.

You can set duration, rest and number of rounds. If you set 0 rounds, timer loops and count number of rounds.
You can add inner timers too.

You can save, load and delete your settings giving a name to them.
                ")
            }

            SectionHeader { text: qsTr("Developers and sources") }
            LinkedLabel {
                anchors {
                    left: parent.left
                    right: parent.right

                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }

                wrapMode: Text.Wrap

                color: Theme.primaryColor
                linkColor: Theme.highlightColor

                font.pixelSize: Theme.fontSizeSmall
                plainText: qsTr("
Copyright (C) 2017-2019 Claudio Maradonna
Unitoo Team, Italy

Sources at: https://gitlab.com/unitoo/harbour-boxing-timer
                ")

                textFormat: Text.RichText
            }

            Repeater {
                model: ListModel {
                    ListElement { label: "Email" ; value: "claudio@unitoo.pw" }
                    ListElement { label: "Github"; value: "UnitooTeam" }
                    ListElement { label: "Diaspora" ; value: "claudiounitoo@framasphere.org" }
                    ListElement { label: "Mastodon" ; value: "@unitoo" }
                    ListElement { label: "XMPP" ; value: "unitoo@fysh.in" }
                }

                delegate: Row {
                    width: aboutPage.width

                    spacing: Theme.paddingLarge

                    Label {
                        width: parent.width * 0.2; height: 25

                        font.pixelSize: Theme.fontSizeTiny
                        text: label

                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter

                        color: Theme.secondaryColor

                        textFormat: Text.RichText
                    }

                    Label {
                        height: 30

                        font.pixelSize: Theme.fontSizeSmall
                        text: value

                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }

            SectionHeader { text: qsTr("Artists") }
            LinkedLabel {
                anchors {
                    left: parent.left
                    right: parent.right

                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }

                wrapMode: Text.Wrap

                color: Theme.primaryColor
                linkColor: Theme.highlightColor

                font.pixelSize: Theme.fontSizeSmall
                plainText: qsTr("
Stefano Amandonico
GrafixPrint, Italy

info@grafixprint.it
http://www.grafixprint.it
                ")

                textFormat: Text.RichText
            }

            SectionHeader { text: qsTr("License") }
            LinkedLabel {
                anchors {
                    left: parent.left
                    right: parent.right

                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }

                wrapMode: Text.Wrap

                color: Theme.primaryColor
                linkColor: Theme.highlightColor

                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.<br><br>

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.<br><br>

You should have received a copy of the GNU General Public License
along with this program.  If not, see <a href=\"http://www.gnu.org/licenses\">http://www.gnu.org/licenses</a>.<br><br><br>


All artworks are licensed under a <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International</a>
                ")

                textFormat: Text.RichText
            }
        }
    }
}
