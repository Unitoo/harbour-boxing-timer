function pushPresetListPage() {
    if (settings.presetsList().length > 0) {
        pageStack.pushAttached(Qt.resolvedUrl("../PresetList.qml"))
    }
}

function playPauseTimer() {
    if (boxingTimer.isActive) {
        boxingTimer.status = BoxingTimer.Pause
    } else {
        boxingTimer.restore()
    }
}

function addSliderForInnerTime() {
    var newArray = settingsPage.mListOfInnerTimers
    newArray.push(5)

    settingsPage.mListOfInnerTimers = newArray

    generateSlidersForInnerTimers()
}

function removeLastSliderFromInnerTimers() {
    var newArray = settingsPage.mListOfInnerTimers
    newArray.pop()

    settingsPage.mListOfInnerTimers = newArray

    generateSlidersForInnerTimers()
}

function getListOfInnerTimers() {
    var list = []

    var timers = settingsPage.mListOfInnerTimers.length
    for (var i = 0; i < timers; i++) {
        list.push(settingsPage.mListOfInnerTimers[i])
    }

    return list;
}

function updateValueInnerTimer(i, value) {
    var newArray = settingsPage.mListOfInnerTimers
    newArray[i] = value

    settingsPage.mListOfInnerTimers = newArray
}

function generateSlidersForInnerTimers() {
    for (var i = 0; i < settingsColumn.children.length; i++) {
        if (settingsColumn.children[i].objectName === 'innerSlider') {
            settingsColumn.children[i].destroy()
        }
    }

    var value = 5
    var remainingTimeForInner = settingsPage.mRoundMs
    var numberOfInnerTimers = settingsPage.mListOfInnerTimers.length
    var newArray = []

    for (i = 0; i < numberOfInnerTimers; i++) {
        value = settingsPage.mListOfInnerTimers[i]
        if (value === undefined) {
            value = 5
        }

        remainingTimeForInner -= value

        var enabled = "true"
        if ((i + 1) < numberOfInnerTimers) {
            enabled = "false"
        }

        var newSlider = Qt.createQmlObject(
            "import QtQuick 2.2;" +
            "import Sailfish.Silica 1.0;" +
            "import 'converter.js' as Converter;" +
            "import 'helpers.js' as Helper;" +
            "Slider {" +
                "objectName: 'innerSlider';" +
                "enabled: "+ enabled +"; "+

                "width: parent.width;" +

                "minimumValue: Converter.sToMs(5);" +
                "maximumValue: "+ remainingTimeForInner +";" +
                "stepSize: Converter.sToMs(5);" +

                "value: "+ parseInt(value) +";" +
                "valueText: Converter.msToTime(value);" +

                "label: qsTr('"+ (i + 1) +"° inner time');" +

                "onValueChanged: Helper.updateValueInnerTimer("+ i +", value);" +
            "}",
            settingsColumn,
            "dynamicInnerTimerSlider"
        );

        newArray.push(value);
    }

    settingsPage.mListOfInnerTimers = newArray;
}
