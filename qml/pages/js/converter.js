function msToS(ms) {
    return ms / 1000;
}

function sToMs(s) {
    return s * 1000;
}

function msToTime(ms) {
    var d = new Date(ms);
    var s = "";

    var minutes = d.getMinutes();
    var seconds = d.getSeconds();

    if (minutes > 0) {
        s += minutes + "m ";
    }

    if (seconds > 0) {
        s += seconds + "s";
    }

    return s;
}
